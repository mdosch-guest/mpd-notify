# mpd-notify

A little tool that prints the song currently played by mpd to the terminal. 
Based on the [libmpdclient example](https://github.com/MusicPlayerDaemon/libmpdclient/blob/master/src/example.c).

## requirements

* [libmpdclient](https://www.musicpd.org/)

## installation


```
git clone https://salsa.debian.org/mdosch/mpd-notify.git
cd mpd-notify
make 
make install
```
