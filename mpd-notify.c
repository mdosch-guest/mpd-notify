/* Based on https://github.com/MusicPlayerDaemon/libmpdclient/blob/master/src/example.c
libmpdclient
(c) 2003-2018 The Music Player Daemon Project
This project's homepage is: http://www.musicpd.org

mpd-notify
Copyright (C) 2019  Martin Dosch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <mpd/client.h>

static int
handle_error(struct mpd_connection *c)
{
	assert(mpd_connection_get_error(c) != MPD_ERROR_SUCCESS);

	fprintf(stderr, "%s\n", mpd_connection_get_error_message(c));
	mpd_connection_free(c);
	return EXIT_FAILURE;
}

static void
print_tag(const struct mpd_song *song, enum mpd_tag_type type,
	  const char *label)
{
	unsigned i = 0;
	const char *value;
	while ((value = mpd_song_get_tag(song, type, i++)) != NULL)
		printf("%s: %s\n", label, value);
}

int main(void){
	while(1) {
		struct mpd_connection *conn;
		conn = mpd_connection_new(NULL, 0, 3000);

		if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
			return handle_error(conn);

		enum mpd_idle idle = mpd_run_idle(conn);
			if (idle == 0 &&
				mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
				return handle_error(conn);

			if (idle & MPD_IDLE_MIXER) {

				struct mpd_status * status;

				mpd_command_list_begin(conn, true);
				mpd_send_status(conn);
				mpd_send_current_song(conn);
				mpd_command_list_end(conn);

				status = mpd_recv_status(conn);
				if (status == NULL)
					return handle_error(conn);

				printf("Vol: %i\n", mpd_status_get_volume(status));

				mpd_status_free(status);
			}

			if ((idle & MPD_IDLE_PLAYER) || (idle & MPD_IDLE_QUEUE)) {

				struct mpd_status * status;
				struct mpd_song *song;

				mpd_command_list_begin(conn, true);
				mpd_send_status(conn);
				mpd_send_current_song(conn);
				mpd_command_list_end(conn);

				status = mpd_recv_status(conn);
				if (status == NULL)
					return handle_error(conn);

				switch(mpd_status_get_state(status)) {
					case MPD_STATE_PLAY:
						if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
						return handle_error(conn);

						mpd_response_next(conn);

						while ((song = mpd_recv_song(conn)) != NULL) {
							print_tag(song, MPD_TAG_ARTIST, "artist");
							print_tag(song, MPD_TAG_ALBUM, "album");
							print_tag(song, MPD_TAG_TITLE, "title");
							print_tag(song, MPD_TAG_NAME, "name");
						/*	printf("volume: %i\n", mpd_status_get_volume(status)); */
							mpd_song_free(song);
						}
						break;
					case MPD_STATE_STOP:
						if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
						return handle_error(conn);
						printf("STOPPED\n");
						break;
					case MPD_STATE_PAUSE:
						if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
						return handle_error(conn);
						printf("PAUSED\n");
						break;
					default:
						printf("UNKNOWN STATE\n");
						break;
					}
				mpd_status_free(status);
			}
	mpd_connection_free(conn);
	}
return 0;
}
